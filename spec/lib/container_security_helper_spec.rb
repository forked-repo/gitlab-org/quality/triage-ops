
# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/container_security_helper'

RSpec.describe ContainerSecurityHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include ContainerSecurityHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'user1' => { 'departments' => ['Protect:Container Security BE Team'] },
      'user2' => { 'departments' => ['Protect:Container Security FE Team'] },
      'user3' => { 'departments' => ['Protect:Container Security BE Team'] },
      'user4' => { 'departments' => ['Protect:Container Security BE Team'] },
      'user5' => { 'departments' => ['Protect:Container Security FE Team'] }
    }
  end

  subject { resource_klass.new(labels) }

  describe '#container_security_be' do
    it 'retrieves team members from www-gitlab-com and returns a random container security backend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)

      expect(subject.container_security_be).to be_in(%w(@user1 @user3 @user4))
    end
  end

  describe '#container_security_fe' do
    it 'retrieves team members from www-gitlab-com and returns a random container security frontend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)

      expect(subject.container_security_fe).to be_in(%w(@user2 @user5))
    end
  end
end
