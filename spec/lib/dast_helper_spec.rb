# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/dast_helper'
require_relative '../../lib/www_gitlab_com'

RSpec.describe DastHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include DastHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'engineer1' => {
        'departments' => ['Secure:Dynamic Analysis BE Team'],
        'specialty' => 'Dynamic Analysis',
        'role' => '<a href="/job-families/engineering/development/backend/#secure">Senior Backend Engineer, Secure:Dynamic Analysis</a>' 
      },
      'manager' => {
        'departments' => ['Secure:Dynamic Analysis BE Team'],
        'specialty' => 'Dynamic Analysis',
        'role' => '<a href="/job-families/engineering/development/backend/#backend-manager-engineering">Backend Engineering Manager, Secure:Dynamic Analysis, Secure:Fuzz Testing</a>' 
      },
      'engineer2' => {
        'departments' => ['Secure:Dynamic Analysis BE Team'],
        'specialty' => 'Dynamic Analysis',
        'role' => '<a href="/job-families/engineering/development/backend/#secure">Senior Backend Engineer, Secure:Dynamic Analysis</a>' 
      },
      'engineer3' => {
        'departments' => ['Secure:Dynamic Analysis FE Team'],
        'specialty' => 'Dynamic Analysis',
        'role' => '<a href="/job-families/engineering/development/backend/#secure">Senior Backend Engineer, Secure:Dynamic Analysis</a>' 
      },
    }
  end

  subject { resource_klass.new(labels) }

  describe '#dast_be' do
    it 'retrieves team members from www-gitlab-com and returns a random dast backend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
      expect(['@engineer1', '@engineer2']).to include(subject.dast_be)
    end
  end
end
