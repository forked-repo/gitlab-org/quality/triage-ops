# frozen_string_literal: true

require_relative File.expand_path('../lib/container_security_helper.rb', __dir__)

Gitlab::Triage::Resource::Context.include ContainerSecurityHelper
